﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class SimpleCtrl : NetworkBehaviour
{
    public float speed = 8.0F;
    public float rotateSpeed = 8.0F;

    [SerializeField]
    Text loseText;

    bool isLost = false;

    void Start()
    {
        if (hasAuthority)
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }
    }

    void Update()
    {
        if (!hasAuthority)
        {
            return;
        }

        if (transform.position.y < -10 && isLost == false)
        {
            var canvas = GameObject.FindObjectOfType<Canvas>();
            var lostTextInstance = Instantiate(loseText, canvas.transform);
            lostTextInstance.GetComponent<RectTransform>().anchoredPosition = new Vector2(0.5f, 0.5f);
            isLost = true;
        }

        CharacterController controller = GetComponent<CharacterController>();
        transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
        Vector3 forward = transform.TransformDirection(Vector3.forward);
        float curSpeed = speed * Input.GetAxis("Vertical");
        controller.SimpleMove(forward * curSpeed);
    }
}